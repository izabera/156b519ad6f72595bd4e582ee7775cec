#!/bin/sed -f

# store rule in hold space
1 { h; d; }

# make borders wrap
s/\(.\)\(.*\)\(.\)/>\3\1\2\3\1|/

# do some mucking around that can probably be done more elegantly by someone who is actually good at sed
x; G; h; s/\n.*//; x

:loop
/>111/ { /^0/        bzero; bone; }
/>110/ { /^.0/       bzero; bone; }
/>101/ { /^..0/      bzero; bone; }
/>100/ { /^...0/     bzero; bone; }
/>011/ { /^....0/    bzero; bone; }
/>010/ { /^.....0/   bzero; bone; }
/>001/ { /^......0/  bzero; bone; }
/>000/ { /^.......0/ bzero; bone; }
bend

# output one character, move cursor forwards, repeat
:zero
s/>\(.\)\(.*\)/\1>\20/; bloop
:one
s/>\(.\)\(.*\)/\1>\21/; bloop

:end
s/.*|//
